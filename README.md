## Getting Started

Welcome to the VS Code Java world. Here is a guideline to help you get started to write Java code in Visual Studio Code.

## Folder Structure

The workspace contains two folders by default, where:

- `src`: the folder to maintain sources
- `lib`: the folder to maintain dependencies

## Dependency Management

The `JAVA DEPENDENCIES` view allows you to manage your dependencies. More details can be found [here](https://github.com/microsoft/vscode-java-pack/blob/master/release-notes/v0.9.0.md#work-with-jar-files-directly).


///////////////*************************////////////////////////////

per la parte grafica, guardare il pdf: gridbaglayout.pdf


1) da sistemare la stampa dei risultati perchè il ciclo controlla lo zero per arrestarsi
2) creare una classe per gli zeri/min/max/flessi!!
3) si presta per il polimorfismo
4) lunghezza dinamica: List 
