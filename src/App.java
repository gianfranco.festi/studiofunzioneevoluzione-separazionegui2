import javax.swing.*;

public class App {

     public static void main(String[] args) throws Exception {
          Grafico g = new Grafico();

          SwingUtilities.invokeLater(new Runnable() {
               public void run() {
                    g.visualizza();
               }
          });
     }
}