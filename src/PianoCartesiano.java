import java.awt.*;
import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.*;
import java.awt.Color;
import java.awt.event.MouseEvent;

public class PianoCartesiano extends JPanel {

    double xOrigine = 0, yOrigine = 0;
    double scale = 1, maxY = 0;

    public PianoCartesiano() {
        MouseListen ml = new MouseListen();
        addMouseListener(ml);
        addMouseMotionListener(ml);
        addMouseWheelListener(ml);
    }

    public PianoCartesiano(double xOrigine, double yOrigine) { // origine assi
        this.xOrigine = xOrigine;
        this.yOrigine = yOrigine;
    }

    public void setxOrigine(double xOrigine) {
        this.xOrigine = xOrigine;
    }

    public void setyOrigine(double yOrigine) {
        this.yOrigine = yOrigine;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

    public void setMaxY(double maxY) {
        this.maxY = maxY;
    }

    private static final long serialVersionUID = 1L;
    int width, height;
    int mar = 50;
    Graphics2D g1;

    public void assi(Graphics2D g1) {
        if (g1 == null)
            return;
        g1.draw(new Line2D.Double(mar - (xOrigine * scale) - 2, mar - (yOrigine * scale), mar - (xOrigine * scale) - 2,
                (height - mar)));
        g1.draw(new Line2D.Double(0, (height - mar) - (yOrigine * scale) - 2, (width),
                (height - mar) - (yOrigine * scale) - 2));
        g1.setPaint(Color.RED);// colore funzione
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g1 = (Graphics2D) g;
        g1.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        width = getWidth();
        height = getHeight();
        scale = (double) (height - 2.0 * mar) / getMax();
        g1.setPaint(Color.BLACK);
    }

    public void plotFx(Punto datiXY[], Graphics2D g1) {
        if (g1 == null)
            return;
        for (int i = 0; i < datiXY.length - 1; i++) {
            double x1 = mar - (xOrigine * scale) + datiXY[i].getX() * scale;
            double y1 = (height - mar) - (yOrigine * scale) - scale * datiXY[i].getY();
            g1.fill(new Ellipse2D.Double(x1 - 2, y1 - 2, 2, 2));
            // g1.draw(new Line2D.Double(x1, y1, x2, y2));
        }
    }

    public void area(Punto datiXY[], Graphics2D g1) {
        if (g1 == null)
            return;
        double x1 ;
        double y1,y2 ;
        double x2;    
        int[] xp  = new int[4];
		int[] yp  = new int[4];
        g1.setColor(Color.RED);
        int i =0,passo=1;
        while (i<(datiXY.length-2*passo) ) {
            x1 = mar - (xOrigine * scale) + datiXY[i].getX() * scale;
            x2 = mar - (xOrigine * scale) + datiXY[i+passo].getX() * scale;
            y1 = (height - mar) - (yOrigine * scale) - scale * datiXY[i].getY();
            y2 = (height - mar) - (yOrigine * scale) - scale * datiXY[i+passo].getY();

            xp[0]=(int)x1;
            xp[1]=(int)x1;
            xp[2]=(int)x2;
            xp[3]=(int)x2;

            yp[0]=(int)((height - mar) - (yOrigine * scale));
            yp[1]=(int)y1;
            yp[2]=(int)y2;
            yp[3]=(int)((height - mar) - (yOrigine * scale));
            if (y2<yp[0] || y1<yp[0])    
                g1.setColor(Color.BLUE);
            else
                g1.setColor(Color.YELLOW);    

            g1.fillPolygon(xp,yp,4);
            i=i+passo;
        }
    }


    private double getMax() {
        return ((this.maxY));
    }

    class MouseListen extends MouseInputAdapter {
        Point p1, p2;
        double distance;


        public void mouseDragged(MouseEvent e) {
            System.out.println("mouseDragged");
            p2 = new Point(e.getPoint());
             xOrigine += (p1.x - p2.x) / scale;
            yOrigine += (p1.y - p2.y) / scale;
            p1.x=p2.x;
            p1.y=p2.y;

            repaint();

        }

        public void mousePressed(MouseEvent e) {
            p1 = new Point(e.getPoint());
            repaint();
        }

        public void mouseWheelMoved(MouseWheelEvent e) {

            if (e.getWheelRotation() > 0) {
                if (maxY <= 0.01)
                    return;
                else
                    maxY += 0.1;
            } else
                maxY -= 0.1;
            repaint();
        }
    }
}
