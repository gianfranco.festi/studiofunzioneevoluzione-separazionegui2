public class Punto {
    private double x;
    private double y;
    public Punto(){
        x=0;
        y=0;
    }
    public Punto(double x, double y){
        setX(x);
        setY(y);
    }
    public Punto(Punto p){
        setX(p.getX());
        setY(p.getY());
    }
    public double getX() {
        return x;
    }
    public void setX(double x) {
        this.x = x;
    }
    public void setY(double y) {
        this.y = y;
    }
    public double getY() {
        return y;
    }
    public String toString(){
        return String.format("8%s x:%9.3f, y:%9.3f ", x, y);
    }
}
