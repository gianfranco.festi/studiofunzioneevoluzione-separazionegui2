import java.awt.*;

public class Risultati {
    public static void plot(
            Graphics g1, 
            String f,
            Zeri z,

            int x,
            int y,double area) {
        g1.drawString("La funzione è: " + f, x, y);
        g1.drawString("Le radici sono a: " + toString(z,"radice"), x, y + 30);
        g1.drawString("I minimi sono a: " + toString(z,"min"), x, y + 45);
        g1.drawString("I massimi sono a: " + toString(z,"max"), x, y + 60);
        g1.drawString("I flessi sono a: " + toString(z,"flesso"), x, y + 75);
        g1.drawString("l'area è: " + area, x, y + 90);
    }

    public static String toString(Zeri z,String tipo) {
        String s = "";
        for (int i = 0; i < z.getLung(); i++) {
            if (z.getZeri(i)!=null) {
                if(z.getZeri(i).getTipo().equals(tipo)){
                    s += "x --> " + String.format("%4.3f", z.getZeri(i).getValore()) + " | ";
                }
            }
        }
        s += System.lineSeparator();
        return s;
    }
}
