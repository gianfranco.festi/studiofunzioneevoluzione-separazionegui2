
import java.util.function.*;

public class StudioFunzione{
    private double a;
    private double b;
    private double epsilon;
    private Punto datiXY[];
    private Zeri z=new Zeri();

    private double minY;
    private double maxY;
    private double area=0;

    public StudioFunzione(double a, double b, double h, double e, Function<Double, Double> f,
            Function<Double, Double> f1, Function<Double, Double> f2, Function<Double, Double> f3) {
        setA(a);
        setB(b);
        this.epsilon = e;

        datiXY= new Punto[(int) Math.ceil(Math.abs(a - b) / h) + 1];
        minY = Double.MAX_VALUE;
        maxY = Double.MIN_VALUE;
        double y = 0;
        int i = 0;
        double yPrec = f.apply(a);
        double xPrec = a;
        for (double x = a; x < b; x += h) {
            y = f.apply(x);
            if (y * yPrec < 0) {
                z.add(trovoRadici(xPrec, x, f, f1), "radice");
            }
            if (f1.apply(xPrec) < 0 && f1.apply(x) > 0) {
                z.add(trovoRadici(xPrec, x, f1, f2), "min");
            } else if (f1.apply(xPrec) > 0 && f1.apply(x) < 0) {
                z.add(trovoRadici(xPrec, x, f1, f2),"max");
            }
            if ((f2.apply(xPrec) * f2.apply(x)) < 0) {
                z.add(trovoRadici(xPrec, x, f2, f3),"flesso");
            }
            area+=(f.apply(x)+f.apply(x+h))*(h/2);
            yPrec = y;
            xPrec = x;
            datiXY[i]=new Punto(x,y);
            if (y > maxY) {
                maxY = y;
            }
            if (y < minY) {
                minY = y;
            }
            i++;
        }
    }
    public Punto[] getDatiXY() {
        return datiXY;
    }
    public Zeri getZ() {
        return z;
    }
    public double getMaxY() {
        return maxY;
    }
    public double getMinY() {
        return minY;
    }
    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }
    public double getArea() {
        return area;
    }

    private double trovoRadici(double a, double b, Function<Double, Double> f, Function<Double, Double> f1) {
        double x2 = 0;
        double x1 = a;
        do {
            x2 = x1;
            x1 = x1 - f.apply(x1) / f1.apply(x1);
        } while (Math.abs(x1 - x2) >= this.epsilon);
        return x1;
    }
}
