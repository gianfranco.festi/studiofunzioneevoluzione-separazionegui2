import java.awt.*;
import java.util.function.*;

public class StudioFunzioneGui extends PianoCartesiano {
    private static final long serialVersionUID = 1L;
    private Graphics2D g1;
    private String fx;

    private StudioFunzione studFunz;

    public StudioFunzioneGui(String fx, double a, double b, double h, double e, Function<Double, Double> f,
            Function<Double, Double> f1, Function<Double, Double> f2, Function<Double, Double> f3) {
        this.fx = fx;

        studFunz=new StudioFunzione(a, b, h, e,f,f1,f2,f3);
        if (studFunz.getA() > 0) {
            setxOrigine(0);
        } else {
            setxOrigine(studFunz.getA());
        }
        setyOrigine(Math.abs((studFunz.getMaxY() - studFunz.getMinY())) / 2.0);
        if ((studFunz.getB() - studFunz.getA()) > (studFunz.getMaxY() - studFunz.getMinY())) {
            setMaxY(studFunz.getB() - studFunz.getA());
        } else {
            setMaxY(studFunz.getMaxY() - studFunz.getMinY());
        }
    }

    public Graphics2D getG1() {
        return g1;
    }
    public void assi() {
        super.assi(g1);
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g1 = (Graphics2D) g;
        Risultati.plot(g1, this.fx, studFunz.getZ(), 120, 60, studFunz.getArea());

        g1.setPaint(Color.BLUE);
        super.assi(g1);
        super.plotFx(studFunz.getDatiXY(), g1);
        super.area(studFunz.getDatiXY(), g1);
    }

    public void plotFx() {
        super.assi(g1);
        super.plotFx(studFunz.getDatiXY(), g1);
        super.area(studFunz.getDatiXY(), g1);
    }

    public String toString(double[] vett) {
        String s = "";
        for (int i = 0; i < vett.length; i++) {
            if (vett[i] != 0) {
                s += "x --> " + String.format("%4.3f", vett[i]) + " | ";
            }
        }
        s += System.lineSeparator();
        return s;
    }
}
