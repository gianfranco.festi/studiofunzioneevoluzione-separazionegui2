import java.util.*;
public class Zeri {

    private Zero[]zeri;
    public Zeri(){
    }
    public Zeri(int dim){
        zeri=new Zero[dim];
    }
    public Zeri(Zero zeri[]){
        this.zeri=Arrays.copyOf(zeri,zeri.length);//shallow copy
        /*for (int i = 0; i < zeri.length; i++) {
            add(zeri[i].getValore(), zeri[i].getTipo()); //deep copy
        }*/
    }
    
    public Zero getZeri(int ind) {
        return zeri[ind];
    }
    public int getLung(){
        return zeri.length;
    }
    public void add(double x,String tipo){
        if(zeri==null){
            zeri=new Zero[1];
        }
        else{
            zeri=Arrays.copyOf(zeri,zeri.length+1);
        }
        zeri[zeri.length-1]=new Zero(x,tipo);
    }
    public String toString(){
        String s="";
        for(Zero zero: zeri){
            s+=zero.toString()+"\n";
        }
        return s;
    }

}
