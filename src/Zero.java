public class Zero {
    private double valore;
    private String tipo="";

    public Zero(){

    }
    public Zero(double valore,String tipo){
        setValore(valore);
        setTipo(tipo);
    }

    public String getTipo() {
        return tipo;
    }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public double getValore() {
        return valore;
    }
    public void setValore(double valore) {
        this.valore = valore;
    }
    public String toString(){
        return String.format("8%s x:%9.3f ", tipo, valore);
    }
}
